let switchover = function(handlerSelector, elementSelector, handlerActiveClassName, elementActiveClassName) {

    // 根据第一个参数选择一批元素，比如匹配于 .tab-list>li 的所有元素
    let handlers = document.querySelectorAll(handlerSelector);
    console.log(handlers);

    // 根据第一个参数选择一批元素，比如匹配于 .appliances-items>div 的所有元素
    let elements = document.querySelectorAll(elementSelector);
    console.log(elements);

    let listener = function(event) {
        let current = event.target; // 通过事件对象获得事件源(就是事件发生在哪里)
        console.log(current);

        let index = 0;
        // 通过循环查找 当前事件源 对象 在 handlers 中的索引
        for (let i = 0; i < handlers.length; i++) {
            // 若 当前事件源对象 与 handlers 中 下标为 i 的元素相等
            if (current === handlers[i]) {
                index = i; // 则将 handlers 中 的 索引值记录到 index 变量中
                break;
            }
        }

        console.log(index);
        let element = elements[index]; // 根据索引获得相应的受控制元素

        for (let i = 0; i < handlers.length; i++) {
            // 为 handlers 中所有元素 移除 .tab-active
            handlers[i].classList.remove(handlerActiveClassName);
            // 为 elements 中所有元素 移除 .appliances-item-container-active
            elements[i].classList.remove(elementActiveClassName);
        }

        // 为鼠标悬浮的元素添加 .tab-active
        current.classList.add(handlerActiveClassName);
        // 为相应的元素添加 .appliances-item-container-active
        element.classList.add(elementActiveClassName);

    }

    // 为 handlers 中所有元素绑定 mouseover 事件监听器
    for (let i = 0; i < handlers.length; i++) {
        let h = handlers[i];
        // 为 h 对应的元素 绑定 mouseover 事件监听器
        h.addEventListener('mouseover', listener, false);
    }

}