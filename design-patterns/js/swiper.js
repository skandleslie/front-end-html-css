// 选择 匹配于 '.home-hero-swiper dt' 的所有元素
let dts = document.querySelectorAll('.home-hero-swiper dt');
//console.log(dts);

let listener = function(event) {

    // 通过循环遍历 dts 为每个 dt 剔除一个 class-name
    for (let i = 0; i < dts.length; i++) {
        let dt = dts[i];
        // 为 dt 所对应的元素删除指定的 class-name
        dt.classList.remove('home-hero-swiper-active');
    }

    // 通过事件对象来获得被点击的元素
    let current = event.target;

    // 为 current 对应的元素 添加 一个 class-name
    current.classList.add('home-hero-swiper-active');

    // 通过循环遍历 dts 为每个 dt 剔除一个 class-name
    for (let i = 0; i < dts.length; i++) {
        let t = dts[i]; // 从 dts 中获取索引是 i 的元素
        let d = t.nextElementSibling; // 获得 t 之后的元素
        d.style.zIndex = '10'; // 将 d 所表示的元素的 z-index 设置为 10
    }

    let next = current.nextElementSibling; // 获得 current 之后的元素
    next.style.zIndex = '11'; // 将 next 对应的元素的 z-index 设置为 11
}

// 用 for 循环 遍历 dts 
for (let i = 0; i < dts.length; i++) {
    let dt = dts[i];
    //console.log(dt);
    // 为每个dt都绑定点击事件对应的监听器
    dt.addEventListener('click', listener, false);
}